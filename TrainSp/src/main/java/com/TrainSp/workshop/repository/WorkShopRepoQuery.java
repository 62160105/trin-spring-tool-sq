package com.TrainSp.workshop.repository;



public class WorkShopRepoQuery {
	
	public static final String findUserDataById  = "SELECT wse.firstname AS Firstname, "
			+ "wse.lastname AS Lastname, wse.name AS Nickname, wse.gender AS Gender,"
			+ "wse.position AS Position, wse.team AS Team\r\n"
			+ "FROM WorkShopEntity wse\r\n"
			+ "WHERE wse.id = :userId";

}
