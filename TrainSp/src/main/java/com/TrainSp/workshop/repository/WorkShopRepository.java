package com.TrainSp.workshop.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.TrainSp.workshop.entity.WorkShopEntity;

import java.util.*;

@Repository
@Transactional
public interface WorkShopRepository extends JpaRepository<WorkShopEntity, Integer>{

	@Query(value= WorkShopRepoQuery.findUserDataById)
	public List<Map<String, String>> findUserDataById(Integer userId);
	

}
