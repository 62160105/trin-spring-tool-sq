package com.TrainSp.workshop.controller;

import java.text.ParseException;
import java.util.*;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.TrainSp.workshop.entity.WorkShopEntity;
import com.TrainSp.workshop.model.WorkShopRequest;
import com.TrainSp.workshop.model.WorkShopResponse;
import com.TrainSp.workshop.service.WorkShopService;

//import com.votesystem.login.model.LoginRequest;
//import com.votesystem.login.service.LoginService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/Train")
@Api(value = "Train", description = "Train Service", tags = { "Train" })
public class WorkShopController {

	@Autowired
	private WorkShopService workshopService;

	@ApiOperation(value = "Find Data")
	@GetMapping(value = "/findUserDataById")
	public List<Map<String, String>> findUserDataById(@RequestParam Integer userId)
			throws ParseException {
		return workshopService.findUserDataById(userId);
	}
	
	@ApiOperation(value = "Find Demo Info by Criteria")
	@PostMapping(value = "/findDemoByCriteria",produces = {"application/json"})
	public List<Map<String, String>> findDemoByCriteria(@RequestParam Integer userId) throws ParseException
	{
		return null;
	}
	
	@ApiOperation(value = "Create Data")
	@PutMapping(value = "/saveData")
	public boolean saveUserData(HttpServletRequest request,
			@RequestBody WorkShopRequest workshopRequest) throws ParseException
	{
		return workshopService.saveUserData(workshopRequest);
	}
	
	@ApiOperation(value = "Delete Data")
	@DeleteMapping(value = "/deleteData")
	public boolean deleteDemo(HttpServletRequest request,
			@RequestParam Integer userId) throws ParseException
	{
		return workshopService.deleteDemo(userId);
	}

}
