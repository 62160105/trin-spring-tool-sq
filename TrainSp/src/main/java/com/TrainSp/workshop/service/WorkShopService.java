package com.TrainSp.workshop.service;

import java.text.ParseException;
import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.TrainSp.workshop.entity.WorkShopEntity;
import com.TrainSp.workshop.model.WorkShopRequest;
import com.TrainSp.workshop.model.WorkShopResponse;
import com.TrainSp.workshop.repository.WorkShopRepository;

@Service
public class WorkShopService {

	@Autowired
	WorkShopRepository workshopRepository;
	
	public List<Map<String, String>> findUserDataById(Integer userId) throws ParseException {
		List<Map<String, String>> results = workshopRepository.findUserDataById(userId);
		return results;
	}
	
	public boolean saveUserData(WorkShopRequest workshopRequest) throws ParseException {
		WorkShopEntity demo = new WorkShopEntity();

		demo.setFirstname(workshopRequest.getFirstname());
		demo.setLastname(workshopRequest.getLastname());
		demo.setGender(workshopRequest.getGender());
		demo.setName(workshopRequest.getName());
		demo.setPosition(workshopRequest.getPosition());
		demo.setTeam(workshopRequest.getTeam());
		workshopRepository.save(demo);
		return true;
	}
	
	public boolean deleteDemo(Integer userId) throws ParseException {
		workshopRepository.deleteById(userId);
		
		return true;
	}
	
}
