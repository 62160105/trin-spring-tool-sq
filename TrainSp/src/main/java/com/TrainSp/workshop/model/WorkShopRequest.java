package com.TrainSp.workshop.model;


import lombok.Data;

@Data
public class WorkShopRequest {

	String firstname;
	String lastname;
	String name;
	String gender;
	String position;
	String team;
	
}
