package com.TrainSp.workshop.model;

import lombok.Data;

@Data
public class WorkShopResponse {

	Integer demoId;
	String data1;
	String data2;
	String user;
	
}
