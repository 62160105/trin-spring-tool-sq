CREATE TABLE public.information (
	id int4 NOT NULL DEFAULT nextval('user_seq'::regclass),
	"name" text NOT NULL,
	firstname text NOT NULL,
	lastname text NOT NULL,
	gender text NOT NULL,
	"position" text NOT NULL,
	team text NOT NULL,
	CONSTRAINT information_pkey PRIMARY KEY (id)
);